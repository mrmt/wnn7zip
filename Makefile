#	convert ken_all.csv into wnn dictionary
#	morimoto@marib.imagica.co.jp
#	Thu Feb 19 17:40:59 JST 1998

# 例えば nkf 1.62 など, -Z, -X オプションが処理できるものを使う
NKF			=	nkf

# Wnn のユーティリティのパス
WNNBINDIR	=	/usr/local/bin/Wnn4

# Wnn の辞書のパス(hinsi.data のあるディレクトリ)
WNNDICDIR	=	/usr/local/lib/wnn/ja_JP

# 郵便番号辞書インストール先
ZIPDICDIR	=	$(WNNDICDIR)/dic/zip

################################################################

DICS		=	7zip1.dic 7zip2.dic 7zip3.dic

all: $(DICS)

install: $(DICS)
	mkdir -p $(ZIPDICDIR)
	cp $(DICS) $(ZIPDICDIR)
touch:
	cd $(ZIPDICDIR) ; $(WNNBINDIR)/wnntouch $(DICS)
xaa xab xac:
	$(NKF) -XeZ ken_all.csv | perl csv2dic | split --lines 50000
7zip1.dic: xaa
	cat $? | $(WNNBINDIR)/atod -S -N -n -s 200000 -h $(WNNDICDIR)/hinsi.data $@
7zip2.dic: xab
	cat $? | $(WNNBINDIR)/atod -S -N -n -s 200000 -h $(WNNDICDIR)/hinsi.data $@
7zip3.dic: xac
	cat $? | $(WNNBINDIR)/atod -S -N -n -s 200000 -h $(WNNDICDIR)/hinsi.data $@

clean:
	rm -f xaa xab xac $(DICS)

# EOF
